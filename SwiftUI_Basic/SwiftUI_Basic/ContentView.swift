//
//  ContentView.swift
//  SwiftUI_Basic
//
//  Created by sang yeob lee on 2022/07/04.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            //Color.cyan  // background color
            Image("kimmimyo_7_wallpaper")
                .resizable()    // 일단 화면 꽉차게
                .edgesIgnoringSafeArea(.all)    // 노치영역까지 사용
                // 설정에 대한 메소드는 아랫줄에 사용하는 것이 관례
            
            VStack(spacing:0){ // 세로로 쌓는 스택의 여백 spacing
                HStack{
                    Text("Hello, world1")
                        .font(.title)
                    Text("HStack은 가로로 쌓는 스택")
                }.padding() // 패딩 여백
                
                Text("Hello, text function \n ZStack으로 레이아웃을 입체적으로 쌓고, \n VStack으로 세로로 쌓는다.")
                    .font(.body)
                    .underline()
                    .bold()
                
                    .frame(width: 300, height: 100)
                    //.padding(.top, 100)
                
                    .foregroundColor(Color.white)
                    .background(Color.black)
                    .cornerRadius(20)
                
                    .lineLimit(2)
                    .truncationMode(.middle)
                    .lineSpacing(10)
                
                Image("스크린샷 2022-05-12 오후 4.59.48")
                    //.edgesIgnoringSafeArea(.all)
                    .resizable()    // 크기에 대한 사이즈 조절을 따라가겠다
                    .frame(/*width: 300, */height: 300)
                    //.padding()
                    /*.mask(
                        Circle()
                    )*/
                
                Image(systemName: "pencil.tip.crop.circle") // sf symbols에서 가져옴
                    .resizable()
                    .frame(width: 100, height: 100)
                    .foregroundColor(.red)
                
                VStack(){
                    Rectangle()
                        .fill(Color.brown) // background 지정
                        //.frame(width: 100, height: 100)
                    
                    Circle()
                        //.frame(width: 200, height: 100)
                    Capsule()
                        //.frame(width: 200, height: 100)
                    Ellipse()
                        //.frame(width: 200, height: 100)
                }.frame(width: 200, height: 100) // 반복되는 스타일은 스택에 동일하게 적용 가능
                
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
