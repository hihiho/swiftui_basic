//
//  SwiftUI_BasicApp.swift
//  SwiftUI_Basic
//
//  Created by sang yeob lee on 2022/07/04.
//

import SwiftUI

@main
struct SwiftUI_BasicApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
